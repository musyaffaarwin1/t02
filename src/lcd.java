public class lcd {
    private String status;
    private int volume;
    private int  brightness;
    private String cable;
    private int pilihancable=1;


    public void turnOff(){
        this.status = "LCD Mati";
    }
    public void turnOn(){
        this.status = "LCD Nyala";
    }
    public void freeze(){
        this.status = "LCD Freeze";
    }
    public void volumeUp(){
        this.volume =+1;
    }
    public void volumeDown(){
        this.volume =-1;
    }
    public void setVolume(int volume){
        this.volume=volume;
    }
    public void brightnessUp(){
        this.brightness=+1;
    }
    public void brightnessDown(){
        this.brightness=-1;
    }
    public void setBrightness(int brightness){
        this.brightness=brightness;
    }
    public void cableUp(){
        this.pilihancable++;
    }
    public void cableDown(){
        this.pilihancable--;
    }
    public void lcdcable(){
        switch(pilihancable){
            case 1:
                cable = "HDMI";
                break;
            case 2:
                cable = "VGA";
                break;
            case 3:
                cable = "DVA";
                break;
            default:
                cable = "Tidak terdeteksi";
                break;
        }
    }
    public void setCable(String Cable){
        this.cable=cable;
    }
    public void displaymessege(){
        System.out.println("---------------LCD---------------");
        System.out.println("Status      : " + status);
        System.out.println("Volume      : " + volume);
        System.out.println("Brightness  : " + brightness);
        System.out.println("Cable       : " + cable);
        System.out.println("---------------------------------");
    }
}
